//
//  SchoolsListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Prashant Pandey on 9/13/23.
//

import Foundation
import Combine
import XCTest
@testable import NYCSchools

final class SchoolsListViewModelTests: XCTestCase {
    
    func test_updateSchoolListWhenOnViewLoad() {
        let apiService = MockAPIService()
        apiService.stub(for: SchoolDirectoryRequest.self) { _ in
            Result<[School], APIServiceError>.Publisher(
                [School.init(
                    dbn: "21K728", schoolName: "Liberation Diploma Plus High School", overview: "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.", location: "2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)", fax: "718-946-6825", email: "scaraway@schools.nyc.gov", website: "schools.nyc.gov/schoolportals/21/K728", interest: "Humanities & Interdisciplinary", city: "Brooklyn"
                )]
            ).eraseToAnyPublisher()
        }
        
        let viewModel = makeViewModel(apiService: apiService)
        viewModel.apply(.onViewDidLoad)
        XCTAssertTrue(!viewModel.schools.isEmpty)
    }
    
    func test_serviceErrorWhenOnAppear() {
        let apiService = MockAPIService()
        apiService.stub(for: SchoolDirectoryRequest.self) { _ in
            Result.Publisher(
                APIServiceError.responseError
            ).eraseToAnyPublisher()
        }
        let viewModel = makeViewModel(apiService: apiService)
        viewModel.apply(.onViewDidLoad)
        XCTAssertTrue(viewModel.isErrorShown)
    }
    
    private func makeViewModel(
        apiService: APIServiceType = MockAPIService()) -> SchoolListViewModel {
        return SchoolListViewModel(
            apiService: apiService
        )
    }
}
