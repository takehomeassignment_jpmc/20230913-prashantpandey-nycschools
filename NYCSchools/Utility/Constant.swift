//
//  Constant.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

enum StringConstants: String {
    
    // MARK: String Constants
    case genericError = "Something went wrong! Please try again later"
    case baseURL = "https://data.cityofnewyork.us"
    
    // MARK: Identifiers
    case schoolCellIdentifier = "school_cell"
    case sequeShowDetail = "schoolDetailIdentifier"
    
    case detailViewIdentifier = "detailViewId"
    case scoresViewIdentifier = "scoresViewId"
    case contactViewIdentifier = "contactViewId"
}
