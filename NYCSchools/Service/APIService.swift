//
//  APIService.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation
import Combine

protocol APIRequestType {
    associatedtype Response: Decodable
    
    var path: String { get }
    var queryItems: [URLQueryItem]? { get }
}

protocol APIServiceType {
    func response<Request>(from request: Request) -> AnyPublisher<Request.Response, APIServiceError> where Request: APIRequestType
}

struct APIService: APIServiceType {
    
    private let baseURL: URL!
    
    init(baseURL: URL = URL(string: StringConstants.baseURL.rawValue)!) {
        self.baseURL = baseURL
    }
    
    func response<Request>(from request: Request) -> AnyPublisher<Request.Response, APIServiceError> where Request : APIRequestType {
        let pathUrl = URL(string: request.path, relativeTo: baseURL)!
        var urlComponent = URLComponents(url: pathUrl, resolvingAgainstBaseURL: true)!
        urlComponent.queryItems = request.queryItems
        let request = URLRequest(url: urlComponent.url!)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .map { data, urlResponse in data }
            .mapError { _ in APIServiceError.responseError }
            .decode(type: Request.Response.self, decoder: decoder)
            .mapError {error in APIServiceError.parseError(error) }
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
