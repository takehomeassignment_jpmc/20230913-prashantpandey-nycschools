//
//  APIServiceError.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

enum APIServiceError: Error {
    case responseError
    case parseError(Error)
}
