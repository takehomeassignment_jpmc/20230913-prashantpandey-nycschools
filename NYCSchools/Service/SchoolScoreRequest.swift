//
//  SchoolScoreRequest.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

struct SchoolScoreRequest: APIRequestType {
    
    let dbnValue: String?
    typealias Response = [SchoolScore]
    
    var path: String { return "/resource/f9bf-2cp4.json" }
    var queryItems: [URLQueryItem]? {
        return [
            .init(name: "dbn", value: dbnValue),
        ]
    }
}
