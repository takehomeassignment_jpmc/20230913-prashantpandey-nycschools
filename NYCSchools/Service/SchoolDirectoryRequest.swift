//
//  SchoolDirectoryRequest.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

struct SchoolDirectoryRequest: APIRequestType {
    typealias Response = [School]
    
    var path: String { return "/resource/s3k6-pzi2.json" }    
    var queryItems: [URLQueryItem]?
}
