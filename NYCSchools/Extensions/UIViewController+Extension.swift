//
//  UIViewController+Extension.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import UIKit.UIViewController

extension UIViewController {
   
    /// function to show alert
    /// - Parameters:
    ///   - title: String alert title
    ///   - message: String alert message
    ///   - buttonTitle: String button title - Default is "Ok"
    func showAlert(title: String, message: String, buttonTitle:String = "Ok") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Handling of open URLs  outside of application and errors while opening urls
    /// - Parameters:
    ///   - scheme: String scheme
    ///   - urlString: String url to be opened
    ///   - contoller: UIViewController on which alert should be displayed incase of error
    func open(scheme: String, urlString: String?, contoller: UIViewController) {
        guard let urlString = urlString,
              let url = URL(string: "\(scheme)://\(urlString)"),
              UIApplication.shared.canOpenURL(url) else {
            
            contoller.showAlert(title: "Error", message: "Unable to open url")
            return
        }
        UIApplication.shared.open(url)
    }
}
