//
//  UIView+Extension.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import UIKit.UIView

extension UIView {
    
    /// display card shape with corner radius and shadow
    /// - Parameter radius: CGFloat
    func cardView(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.shadowColor = UIColor.brown.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.5
    }
}
