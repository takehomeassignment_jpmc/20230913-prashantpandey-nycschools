//
//  SchoolDetailsViewController.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import UIKit
import Combine
import MapKit

class SchoolDetailsViewController: UIViewController {

    // MARK: IBOutlets

    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: Properties

    lazy var schoolDetailViewModel = SchoolDetailsViewModel(school: school)
    @Published var school: School?
    var cancellable = Set<AnyCancellable>()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = school?.schoolName
        addSubscriptions()
        displayMap()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.schoolDetailViewModel.apply(.onScoreViewDidAppear)
    }
    
    // MARK: Helper Methods
    
    /// Add subscriptions on viewModel's publisher properties and update UI
    func addSubscriptions() {
        /// Subscribe to school from view model
        schoolDetailViewModel.$school
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { _ in
                self.detailTableView?.reloadRows(at: [IndexPath(row: 0, section: 0), IndexPath(row: 2, section: 0)], with: .automatic)
            })
            .store(in: &cancellable)

        /// Subscribe to school score from view model
        schoolDetailViewModel.$schoolScores
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { _ in
                self.detailTableView?.reloadRows(at: [IndexPath(row: 0, section: 0), IndexPath(row: 1, section: 0)], with: .automatic)
            })
            .store(in: &cancellable)
        
        /// Subscribe to error message from view model
        schoolDetailViewModel.$errorMessage
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { errorMessage in
                guard let error = errorMessage else { return }
                self.showAlert(title: "Error", message: error)
            })
            .store(in: &cancellable)
    }
    
    /// display map with location details
    private func displayMap() {
        
        guard let latitude = Double(self.school?.latitude ?? "0"), let longitude = Double(self.school?.longitude ?? "0") else {
            return
        }

        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = school?.schoolName
        mapView.addAnnotation(annotation)
    }

}

// MARK: - TableView Delegate and DataSource
extension SchoolDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifiers = [ StringConstants.detailViewIdentifier.rawValue, StringConstants.scoresViewIdentifier.rawValue, StringConstants.contactViewIdentifier.rawValue
        ]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[indexPath.row]) as? SchoolDetailsCell {
            cell.delegate = self
            switch indexPath.row {
            case 1:
                cell.updateScores(score: self.schoolDetailViewModel.schoolScores?.first)
            case 2:
                cell.updateContactInfo(school: self.school)
            default:
                cell.updateStudents(school: self.school, score: self.schoolDetailViewModel.schoolScores?.first)
            }
            return cell
        }
        return UITableViewCell()
    }
}

// MARK: - SchoolDetailsDelegate

extension SchoolDetailsViewController: SchoolDetailsDelegate {
    func handleUrl(scheme: String, url: String?) {
        self.open(scheme: scheme, urlString: url, contoller: self)
    }
}
