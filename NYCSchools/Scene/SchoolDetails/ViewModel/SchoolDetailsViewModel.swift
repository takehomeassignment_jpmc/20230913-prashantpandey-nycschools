//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation
import Combine

final class SchoolDetailsViewModel: ObservableObject, UnidirectionalFlowType {
    typealias InputType = Input
    
    private var cancellables: [AnyCancellable] = []
    
    // MARK: Input
    enum Input {
        case onScoreViewDidAppear
    }
    
    func apply(_ input: Input) {
        switch input {
        case .onScoreViewDidAppear: onScoreViewDidAppearSubject.send(())
        }
    }
    
    private let onScoreViewDidAppearSubject = PassthroughSubject<Void, Never>()
    
    // MARK: Output
    @Published var school: School?
    @Published private(set) var schoolScores: [SchoolScore]?
    @Published private(set) var errorMessage: String?
    
    private let scoreResponseSubject = PassthroughSubject<[SchoolScore], Never>()
    private let errorSubject = PassthroughSubject<APIServiceError, Never>()
    
    private let apiService: APIService
    
    init(apiService: APIService = APIService(), school: School?) {
        self.apiService = apiService
        self.school = school
        
        bindInputs()
        bindOutputs()
    }
    
    private func bindInputs() {
        let request = SchoolScoreRequest(dbnValue: school?.dbn)
        let responsePublisher = onScoreViewDidAppearSubject
            .flatMap { [apiService] _ in
                apiService.response(from: request)
                    .catch { [weak self] error -> Empty<[SchoolScore], Never> in
                        self?.errorSubject.send(error)
                        return .init()
                    }
            }
        
        let responseStream = responsePublisher
            .share()
            .subscribe(scoreResponseSubject)
        cancellables += [responseStream]
    }
    
    private func bindOutputs() {
        let repositoriesStream = scoreResponseSubject
            .map {
                $0
            }
            .assign(to: \.schoolScores, on: self)
        
        let errorMessageStream = errorSubject
            .map { error -> String in
                switch error {
                case .responseError: return "response error"
                case .parseError: return "parse error"
                }
            }
            .assign(to: \.errorMessage, on: self)
        
        cancellables += [repositoriesStream,
                         errorMessageStream,
        ]
    }
}
