//
//  SchoolListViewController.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import UIKit
import Combine

class SchoolListViewController: UITableViewController, ActivityIndicatorPresenter {
    
    // MARK: Properties
    
    let schoolListViewModel: SchoolListViewModel = SchoolListViewModel()
    var cancellable = Set<AnyCancellable>()
    var isLoadingList: Bool = false
    var activityIndicator = UIActivityIndicatorView()
    var selectedSchool: School?

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubscriptions()
        addRefreshControl()
        updateLoadingIndicator(stop: false)
        self.schoolListViewModel.apply(.onViewDidLoad)
    }
    
    // MARK: - Private functions
    
    //add pull to refresh view
    private func addRefreshControl() {
        refreshControl = UIRefreshControl()
        if let refreshControl = refreshControl {
            refreshControl.tintColor = .systemBackground
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes:[NSAttributedString.Key.foregroundColor: UIColor.systemBackground])
            refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
            tableView.addSubview(refreshControl)
        }
    }
    
    /// refresh school data
    @objc func refresh(_ sender: AnyObject) {
        updateLoadingIndicator(stop: false)
        self.schoolListViewModel.apply(.onPullToRefresh)
    }
    
    // MARK: Helper Methods
    
    /// Add subscriptions on viewModel's publisher properties and update UI
    func addSubscriptions() {
        /// Subscribe to schools array from view model
        schoolListViewModel.$schools
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { _ in
                self.updateLoadingIndicator()
                self.refreshControl?.endRefreshing()
                self.tableView?.reloadData()
            })
            .store(in: &cancellable)
        
        /// Subscribe to error message from view model
        schoolListViewModel.$errorMessage
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { errorMessage in
                guard let error = errorMessage else { return }
                self.refreshControl?.endRefreshing()
                self.updateLoadingIndicator()
                self.showAlert(title: "Error", message: error)
            })
            .store(in: &cancellable)
    }
    
    func updateLoadingIndicator(stop: Bool = true) {
        isLoadingList = !stop
        stop ? hideActivityIndicator() : showActivityIndicator()
    }
}

// MARK: Table View Delegate and Datasource methods

extension SchoolListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolListViewModel.schools.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StringConstants.schoolCellIdentifier.rawValue, for: indexPath) as! SchoolTableViewCell
        cell.school = self.schoolListViewModel.schools[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedSchool = self.schoolListViewModel.schools[indexPath.row]
        return indexPath
    }
}

// MARK: - Navigation

extension SchoolListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        view.endEditing(true)
        if segue.identifier == StringConstants.sequeShowDetail.rawValue {
            guard let schoolDetailViewController = segue.destination as? SchoolDetailsViewController else {
                return
            }
            schoolDetailViewController.school = selectedSchool
        }
    }
}
