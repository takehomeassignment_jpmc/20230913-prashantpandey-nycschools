//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import UIKit
import Combine

class SchoolTableViewCell: UITableViewCell {

    var cancellable = Set<AnyCancellable>()
    
    // MARK: Publishers
    @Published var school: School?
    
    // MARK: IBOutlets
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolInterest: UILabel!
    @IBOutlet weak var schoolCity: UILabel!

    @IBOutlet weak var detailsView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        detailsView.cardView(radius: 5.0)
    }
    
    // MARK: Life Cycle Functions
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        $school
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { value in
                self.schoolName.text = value?.schoolName
                self.schoolInterest.text = value?.interest
                self.schoolCity.text = value?.city
            })
            .store(in: &cancellable)
    }
}
