//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation
import Combine

final class SchoolListViewModel: ObservableObject, UnidirectionalFlowType {
    typealias InputType = Input
    
    private var cancellables: [AnyCancellable] = []
    
    // MARK: Input
    enum Input {
        case onViewDidLoad
        case onPullToRefresh
    }
    
    func apply(_ input: Input) {
        switch input {
        case .onPullToRefresh, .onViewDidLoad: onViewDidAppearSubject.send(())
        }
    }
    
    private let onViewDidAppearSubject = PassthroughSubject<Void, Never>()
    
    // MARK: Output
    @Published private(set) var schools: [School] = []
    @Published private(set) var errorMessage: String?
    @Published var isErrorShown = false
    
    private let responseSubject = PassthroughSubject<[School], Never>()
    private let errorSubject = PassthroughSubject<APIServiceError, Never>()
    
    private let apiService: APIServiceType
    
    init(apiService: APIServiceType = APIService()) {
        self.apiService = apiService
        
        bindInputs()
        bindOutputs()
    }
    
    private func bindInputs() {
        let request = SchoolDirectoryRequest()
        let responsePublisher = onViewDidAppearSubject
            .flatMap { [apiService] _ in
                apiService.response(from: request)
                    .catch { [weak self] error -> Empty<[School], Never> in
                        self?.errorSubject.send(error)
                        return .init()
                    }
            }
        
        let responseStream = responsePublisher
            .share()
            .subscribe(responseSubject)
        cancellables += [responseStream]
    }
    
    private func bindOutputs() {
        let repositoriesStream = responseSubject
            .map {
                $0
                
            }
            .assign(to: \.schools, on: self)
        
        let errorMessageStream = errorSubject
            .map { error -> String in
                switch error {
                case .responseError: return "response error"
                case .parseError: return "parse error"
                }
            }
            .assign(to: \.errorMessage, on: self)
        
        let errorStream = errorSubject
            .map { _ in true }
            .assign(to: \.isErrorShown, on: self)
        
        cancellables += [repositoriesStream,
                         errorMessageStream,
                         errorStream,
        ]
    }
}
