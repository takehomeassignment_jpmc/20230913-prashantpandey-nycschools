//
//  UnidirectionalFlowType.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

protocol UnidirectionalFlowType {
    associatedtype InputType
    func apply(_ input: InputType)
}
