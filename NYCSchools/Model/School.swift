//
//  School.swift
//  NYCSchools
//
//  Created by Prashant Pandey on 9/12/23.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let schoolName: String?
    let overview: String?
    let location: String?
    var latitude: String?
    var longitude: String?
    var totalStudents: String?
    var phone: String?
    let fax: String?
    let email: String?
    let website: String?
    let interest: String?
    let city: String?

    enum CodingKeys: String, CodingKey {
        case dbn, location, latitude, longitude, website, city
        case schoolName = "program1"
        case overview = "overview_paragraph"
        case totalStudents = "total_students"
        case phone = "phone_number"
        case fax = "fax_number"
        case email = "school_email"
        case interest = "interest1"
    }
}
